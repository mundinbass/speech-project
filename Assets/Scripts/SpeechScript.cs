﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class SpeechScript : MonoBehaviour
{

    // Thing to be controlled
    public GameObject puppetCube, puppetSphere;
    public Light lt;

    private KeywordRecognizer keywordRecognizer;
    private Dictionary<string, Action> actions = new Dictionary<string, Action>();


    // Start is called before the first frame update
    void Start()
    {
        actions.Add("cube forward", Forward);
        actions.Add("cube down", Down);
        actions.Add("cube up", Up);
        actions.Add("cube back", Back);
        actions.Add("sphere forward", SphereForward);
        actions.Add("sphere down", SphereDown);
        actions.Add("sphere up", SphereUp);
        actions.Add("sphere back", SphereBack);
        actions.Add("lights off", LightOff);
        actions.Add("lights on", LightOn);

        keywordRecognizer = new KeywordRecognizer(actions.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += RecognizedSpeech;
        keywordRecognizer.Start();

        //lt = GetComponent<Light>();
    }

    private void RecognizedSpeech(PhraseRecognizedEventArgs speech)
    {
        Debug.Log(speech.text);
        actions[speech.text].Invoke();
    }

    private void LightOn()
    {
        lt.intensity = 1.0f;
    }
    private void LightOff()
    {
        lt.intensity = 0.0f;
    }
    private void Forward()
    {
        puppetCube.transform.Translate(1, 0, 0);
    }

    private void Down()
    {
        puppetCube.transform.Translate(0, -1, 0);
    }
    private void Up()
    {
        puppetCube.transform.Translate(0, 1, 0);
    }
    private void Back()
    {
        puppetCube.transform.Translate(-1, 0, 0);
    }

    private void SphereForward()
    {
        puppetSphere.transform.Translate(1, 0, 0);
    }

    private void SphereDown()
    {
        puppetSphere.transform.Translate(0, -1, 0);
    }
    private void SphereUp()
    {
        puppetSphere.transform.Translate(0, 1, 0);
    }
    private void SphereBack()
    {
        puppetSphere.transform.Translate(-1, 0, 0);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
